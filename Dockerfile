# Use an existing docker image as a base
FROM node:alpine

#Select default directory
WORKDIR /usr/app

# Download and install a dependency

#copy build file
COPY ./package.json ./
RUN npm install
#copy app.js and other file
COPY ./ ./

# What to do as a container
CMD ["npm","start"]